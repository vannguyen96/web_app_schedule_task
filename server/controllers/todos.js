const Todo = require('../models/todo');


module.exports = {
    createTodo: async(req,res,next) =>{
        const { name,priority,description,duedate} = req.value.body;
        try {
            let newTodo = await Todo.create({
                name,
                priority: parseInt(priority),
                description,
                duedate
            },{
                fields: ["name","priority","description","duedate"]
            });
            if(newTodo){
                res.json({
                    result: 'ok',
                    data: newTodo
                })
            }
            else
            {
                res.json({
                    result: 'failed',
                    data: {},
                    message:'Insert Todo failed'
                })
            }
        } catch (error) {
            res.json({
                result: 'failed',
                data: {},
                message:'Insert Todo failed. Error: ${error}' +error
            })
        }
    }
}