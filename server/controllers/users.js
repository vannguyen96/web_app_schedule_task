const JWT  = require('jsonwebtoken');
const User = require('../models/user');
const JWT_SECRET = require('../configuration/index')

//generate token
signToken = user =>{
    return JWT.sign({
        iss: 'DMSCodeWorkr',
        sub: user.id,
        iat: new Date().getTime(),
        exp: new Date().setDate(new Date().getDate() +1)
    }, 'VanNguyen');
}


module.exports ={
    signUp: async (req,res,next) => {
        const { email,password } = req.value.body;

        //check user
        const foundUser = await User.findOne({ email })
        if(foundUser){
            return res.status(403).json({ error : 'Email is already in use' })
        }
        const newUser = new User({ email,password});
        await newUser.save();

        //generate the token
        const token = signToken(newUser);

        res.status(200).json({ token });
    },

    signIn: async (req,res,next) => {
        const token = signToken(req.user);
        res.status(200).json({ token });
    },

    secret: async (req,res,next) => {
        console.log('secret call');
        res.json({ secret: "resource"})
    }
}