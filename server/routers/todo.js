const express = require('express');
const router = require('express-promise-router')();

const TodoController = require('../controllers/todos');
const Todo = require('../models/todo');
const repository = require('../repositories/TodoRepository');

// router.route('/createTodo')
//     .post(TodoController.createTodo);

    router.post('/',async (req,res) =>{
        let { name} = req.body;
        repository.create(name).then((todo) => {
            res.json(todo);
          }).catch((error) => console.log(error));
    });

    router.delete('/:id', async (req,res) =>{
        const { id } = req.params;
        repository.deleteById(id).then((ok) => {
            console.log(ok);
            console.log(`Deleted record with id: ${id}`);
            res.status(200).json([]);
        }).catch((error) => console.log(error));
    })


module.exports = router;