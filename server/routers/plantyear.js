const express = require('express');
const router = require('express-promise-router')();

const PlantYear = require('../models/plantyear');
const repository = require('../repositories/PlantYearRepository');

// router.route('/createTodo')
//     .post(TodoController.createTodo);

    router.post('/',async (req,res) =>{
        let { name,description,endDate,completed} = req.body;
        repository.create(name,description,endDate,completed).then((PlantYear) => {
            //res.json(PlantYear);
            res.json({
                result: 'ok',
                data: PlantYear,
                message:'Insert PlantYear successfully'
            })
            }).catch((error) => res.json({
                result: 'failed',
                data: {},
                message:'Insert PlantYear failed' + error
            }));
    });

    router.delete('/:id', async (req,res) =>{
        const { id } = req.params;
        repository.deleteById(id).then((ok) => {
            console.log(ok);
            console.log(`Deleted record with id: ${id}`);
            res.status(200).json([]);
        }).catch((error) => console.log(error));
    })

    router.put('/:id', async (req,res) =>{
        const { id } = req.params;
        const plantYear = { name: req.body.name,description: req.body.description,endDate: req.body.endDate,completed: req.body.completed,done: req.body.done}
        repository.updateById(id,plantYear).then((ok) =>{
            console.log(ok);
            console.log(`Update record with id: ${id}`);
            res.status(200).json([]);
        }).catch((error) => console.log(error));
    })

    router.get('/', async (req,res) =>{
        repository.findAll().then((PlantYear) => {
            res.json({
                result: 'ok',
                data: PlantYear,
                lengh: PlantYear.lengh,
                message:'get PlantYear successfully'
            })
          }).catch((error) => res.json({
            result: 'failed',
            data: {},
            message:'get PlantYear failed' + error
        }));
    })

    router.get('/:id', async (req,res) =>{
        const { id } = req.params;
        repository.findById(id).then((PlantYear) => {
            res.json({
                result: 'ok',
                data: PlantYear,
                lengh: PlantYear.lengh,
                message:'get PlantYear successfully'
            })
          }).catch((error) => res.json({
            result: 'failed',
            data: {},
            message:'get PlantYear failed' + error
        }));
    })


module.exports = router;