const PlantYear = require('../models/plantyear');

class PlantYearRepository {

  constructor(model) {
    this.model = model;
  }

  // create a new todo
  create(name,description,endDate,completed) {
    const newPlantYear = { name,description,endDate,completed, done: false };
    const plantyear = new this.model(newPlantYear);

    return plantyear.save();
  }

  // return all todos

  findAll() {
    return this.model.find();
  }

  //find todo by the id
  findById(id) {
    return this.model.findById(id);
  }

    // delete todo
  deleteById(id) {
    return this.model.findByIdAndDelete(id);
  }

  //update todo
  updateById(id, object) {
    const query = { _id: id };
    return this.model.findOneAndUpdate(query, { $set: { name: object.name,description: object.description,endDate: object.endDate,completed: object.completed, done: object.done } });
  }
}

module.exports = new PlantYearRepository(PlantYear);