const mongoose = require('mongoose');

const { Schema } = mongoose;

// Define schema for todo items
const plantyearSchema = new Schema({
  name: {
    type: String,
  },
  description: {
    type: String
  },
  endDate: {
    type: Date
  },
  completed: {
    type: Number
  },
  done: {
    type: Boolean,
  },
});

const PlantYear = mongoose.model('PlantYear', plantyearSchema);

module.exports = PlantYear;