const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const cron = require('node-cron');
const nodemailer = require('nodemailer');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/APIAuthentication');

const app = express();

app.use(cors());

//middlewares
app.use(morgan('dev'));
app.use(bodyParser.json());

//routes
app.use('/users', require('./routers/users'));
app.use('/todos',require('./routers/todo'));
app.use('/plantyears',require('./routers/plantyear'));

//mail
var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'vannguyenfit@gmail.com',
      pass: 'Chinh@12341234'
    }
  });
  
  var mailOptions = {
    from: 'vannguyenfit@gmail.com',
    to: 'vannt@dmsc.com.vn',
    subject: 'Sending Email using Node.js',
    text: 'That was easy!'
  };
  
  

//

//cron 
// cron.schedule('* * * * *', () => {
//     console.log('running a task every minute');
//     transporter.sendMail(mailOptions, function(error, info){
//         if (error) {
//           console.log(error);
//         } else {
//           console.log('Email sent: ' + info.response);
//         }
//       });
// });

//start the server
// const port = process.env.PORT || 3000;
// app.listen(port);
// console.log("Port: "+ port);

module.exports = app;